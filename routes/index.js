var NodePDF = require('nodepdf');
var email   = require('emailjs');

var html = '<html><head></head><body>Hello, World!</body></body></head></html>';

var pdf = new NodePDF(null, 'public/hello-world.pdf', {
  'content': html,
  'viewportSize': {
    'width': 1440,
    'height': 900
  },
  'args': '--debug=true'
});

var server  = email.server.connect({
  //user:    'username',
  //password:'password',
  host:    'mail.352inc.com',
  ssl:     false
});

var message = {
  text:    'greetings from heroku - i sure hope this works',
  from:    'pop@352media.com',
  to:      'lcarter@352inc.com',
  //cc:      'clangford@352inc.com',
  subject: 'testing emails with attachments from heroku',
  attachment:
    [
      //{path:'public/hello-world.pdf', type:'application/zip', name:'renamed.zip'}
      {path:'public/hello-world.pdf', type:'application/octet-stream', name:'certificate.pdf'}
    ]
};

exports.index = function(req, res){
  res.render('index', { title: 'Express', buyer: 'Bashir Buyer', seller: 'Sayed Seller' });
};

pdf.on('error', function(msg){
  console.log('something has gone horribly wrong!');
  console.log(msg);
});

pdf.on('done', function(pathToFile){
  console.log('pdf file path:  ' + pathToFile);
  server.send(message, function(err, message) {
    //console.log(err || message);
    if(err) {
      console.log('something has gone horribly wrong sending emails');
      console.log(err);
    } else {
      console.log(message);
    }
  });
});

// listen for stdout from phantomjs
pdf.on('stdout', function(stdout){
  // handle
});

// listen for stderr from phantomjs
pdf.on('stderr', function(stderr){
  // handle
});